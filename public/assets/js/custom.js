// Funciones //
function format ( d ) {
  return '<h5>Denunciante</h5>'+
          '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
          '<tr>'+
          '<th>Nombre / Razón Social</th>'+
          '<th>Entidad</th>'+
          '<th>Municipio</th>'+
          '<th>Dirección</th>'+
          '</tr>'+
          '<tr>'+
          '<td>'+d.datos_parte_rel[0].nomParte_razonSocial+'</td>'+
          '<td>'+d.datos_parte_rel[0].entidad_rel.nomEntidad+'</td>'+
          '<td>el municipio</td>'+
          '<td>'+d.datos_parte_rel[0].direccion+'</td>'+
          '</tr>'+
        '</table>'+
        '<h5>Denunciado</h5>'+
          '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
          '<tr>'+
          '<th>Nombre / Razón Social</th>'+
          '<th>Entidad</th>'+
          '<th>Municipio</th>'+
          '<th>Dirección</th>'+
          '</tr>'+
          '<tr>'+
          '<td>'+d.datos_parte_rel[1].nomParte_razonSocial+'</td>'+
          '<td>'+d.datos_parte_rel[1].entidad_rel.nomEntidad+'</td>'+
          '<td>el municipio</td>'+
          '<td>'+d.datos_parte_rel[1].direccion+'</td>'+
          '</tr>'+
        '</table>';
}

var spanish = {
    "sProcessing":     "Procesando...",
    "sLengthMenu":     "Mostrar _MENU_ registros",
    "sZeroRecords":    "No se encontraron resultados",
    "sEmptyTable":     "Ningún dato disponible en esta tabla =(",
    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix":    "",
    "sSearch":         "Buscar:",
    "sUrl":            "",
    "sInfoThousands":  ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst":    '<i class="material-icons miTi">chevron_left</i>',
        "sLast":     '<i class="material-icons miTi">chevron_right</i>',
        "sNext":     '<i class="material-icons miTi">chevron_right</i>',
        "sPrevious": '<i class="material-icons miTi">chevron_left</i>'
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    },
    "buttons": {
        "copy": "Copiar",
        "colvis": "Visibilidad"
    }
  }
/////////////////////////////////

// Ready //
$( document ).ready(function() {        
    
    ///datatable notas///
    var tableDenuncias = $('#tableDenuncias').DataTable({      
        "language":spanish,
        "responsive": true,
        "processing": true,
        "serverSide": true,
        "ajax": "getDenuncias",              
        "columns": [
          { "data":""},
          {"data":"idDenuncia"},
          {"data": "expediente"},
          {"data":"fuente_rel.nomFuente"},
          {"data":"atiende"},                    
          {"data":"fcCrea"},
        ],
        "columnDefs": [
          {
            "targets": 0,
            "data": null,
            "orderable": false,
            "className": 'details-control',
            "defaultContent": ''
          },                   
          {
            "targets": 6,
            "data":"estatus_rel",
            "render":function(dta, type, row){
              if(dta.idEstatus == 1){
                cad = '<p style="color:red">'+dta.nomEstatus+'</p>';
              }
              else if(dta.idEstatus == 2){  
                cad = '<a style="color:blue; background-color:#bfc9ca" class="waves-effect waves-light btn btnOrden">'+dta.nomEstatus+'</a>';
              }
              return cad;
            }
          },    
          
        ],
     });
     $('.dataTables_length select').addClass('browser-default');
     $('#tableDenuncias tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');        
        var row = tableDenuncias.row( tr );
    
        if ( row.child.isShown() ) {
          // This row is already open - close it
          row.child.hide();
          tr.removeClass('shown');
        }
        else {
          // Open this row
          row.child( format(row.data()) ).show();
          tr.addClass('shown');
        }
      });
      //Botones dentro de una fila
      $('#tableDenuncias tbody').on('click','.btnOrden', function(){
        var tr = $(this).closest('tr');  
        var row = tableDenuncias.row(tr).data();
        console.log(row);
        cad = '<div class="modal-content">'+
              '<p><b>id Orden</b>: '+row.orden_rel.idOrden+'</p>'+
              '<p><b>fecha Orden</b>: '+row.orden_rel.fcCrea+'</p>'+
              '</div>'+
              '<div class="modal-footer">'+
              '</div>';
        $('#modalOrden').html(cad)
        $('#modalOrden').openModal();
                
      });
            
}); //ready
/////////////////////////////
/* $(document).on('click', '.eliminaColum', function(e){
    e.preventDefault();            
    $(this).parent('li').remove();
    var id = $(this).data('id');  
    delete columnas[id];
    console.log(columnas);  
  }); */
