function cargarEstado(id_estado){
    //que quiero hacercuando se seleccione un estado //
    console.log('Se selecciono el id_estado: '+id_estado);
    window.location = '/sintesisInformativa/entidad/'+id_estado;
}
function cargarEstadoIc(id_estado){
    //que quiero hacercuando se seleccione un estado //
    console.log('Se selecciono el id_estado: '+id_estado);
    window.location = '/incidencias/entidad/'+id_estado;
}
function goBack() {
    window.history.back();
 }
function destory_editor(selector){
    if($(selector)[0])
    {
        var content = $(selector).find('.ql-editor').html();
        $(selector).html(content);

        $(selector).siblings('.ql-toolbar').remove();
        $(selector + " *[class*='ql-']").removeClass (function (index, css) {
           return (css.match (/(^|\s)ql-\S+/g) || []).join(' ');
        });

        $(selector + "[class*='ql-']").removeClass (function (index, css) {
           return (css.match (/(^|\s)ql-\S+/g) || []).join(' ');
        });
    }
    else
    {
        console.error('editor not exists');
    }
}

// Ready //
$( document ).ready(function() { 
        
    // Mapa dinamico //
    /* State Names */    
    var state_names = new Array();
    var state_class = new Array();
        state_names[1]="Aguascalientes";state_names[2]="Baja California";state_names[3]="Baja California Sur";state_names[4]="Campeche";state_names[5]="Coahuila";state_names[6]="Colima";state_names[7]="Chiapas";state_names[8]="Chihuahua";state_names[9]="Distrito Federal";state_names[10]="Durango";state_names[11]="Guanajuato";state_names[12]="Guerrero";state_names[13]="Hidalgo";state_names[14]="Jalisco";state_names[15]="Estado de M&eacute;xico";state_names[16]="Michoac&aacute;n";state_names[17]="Morelos";state_names[18]="Nayarit";state_names[19]="Nuevo Le&oacute;n";state_names[20]="Oaxaca";state_names[21]="Puebla";state_names[22]="Quer&eacute;taro";state_names[23]="Quintana roo";state_names[24]="San Luis Potos&iacute;";state_names[25]="Sinaloa";state_names[26]="Sonora";state_names[27]="Tabasco";state_names[28]="Tamaulipas";state_names[29]="Tlaxcala";state_names[30]="Veracruz";state_names[31]="Yucat&aacute;n";state_names[32]="Zacatecas";
        state_class[1]="AGU";state_class[2]="BCN";state_class[3]="BCS";state_class[4]="CAM";state_class[5]="COA";state_class[6]="COL";state_class[7]="CHP";state_class[8]="CHH";state_class[9]="DIF";state_class[10]="DUR";state_class[11]="GUA";state_class[12]="GRO";state_class[13]="HID";state_class[14]="JAL";state_class[15]="MEX";state_class[16]="MIC";state_class[17]="MOR";state_class[18]="NAY";state_class[19]="NLE";state_class[20]="OAX";state_class[21]="PUE";state_class[22]="QUE";state_class[23]="ROO";state_class[24]="SLP";state_class[25]="SIN";state_class[26]="SON";state_class[27]="TAB";state_class[28]="TAM";state_class[29]="TLA";state_class[30]="VER";state_class[31]="YUC";state_class[32]="ZAC";
    $(function () {
        $('.listaEdos').mouseover(function(e) {                
            $($(this).data('parent-map')).mouseover();
        }).mouseout(function(e) {                
            $($(this).data('parent-map')).mouseout();                    
        }).click(function(e) { 
            e.preventDefault(); 
            $($(this).data('parent-map')).click(); 
        });

    
        $('.area').hover(function () {
            var id_estado = $(this).data('id-estado');
            var meid = $(this).attr('id');
            $('#edo').html(state_names[id_estado]);                
            $('#letras'+meid).addClass('listaEdosHover');
            $('.escudo').addClass('escudo_img');
            if(last_selected_id_estado!==null){
                $('.escudo').removeClass(state_class[last_selected_id_estado]);
            }
            $('.escudo').addClass(meid);
        }).mouseout(function () {
            var meid = $(this).attr('id');
            $('#letras'+meid).removeClass('listaEdosHover');
            $('.escudo').removeClass(meid);
            if(last_selected_id_estado!==null){
                $('#edo').html(state_names[last_selected_id_estado]);
                $('.escudo').addClass(state_class[last_selected_id_estado]);
            }else{                    
                $('#edo').html("&nbsp;");
                $('.escudo').removeClass('escudo_img');
                //$('.escudo').attr('class','escudo');
            }
        });
        //$('#map_ID').imageMapResize();//funciona perfectamente
        var areaLastClicked=null;
        var last_selected_id_estado=null;
        $('.area').click(function (e) {
                e.preventDefault();
                var $area = $(this);
                //console.log($area)
                var meid = $area.attr('id');
                //console.log(meid)
                //$('.area').mouseout();
                var data = $area.data('maphilight') || {};    
                //console.log(data);                
                if(areaLastClicked!==null){                        
                    var lastData = areaLastClicked.data('maphilight') || {};
                    lastData.alwaysOn=false;
                    $('#letras'+areaLastClicked.attr('id')).removeClass('listaEdosActive');
                    $('.escudo').removeClass(state_class[last_selected_id_estado]);
                }
                $('#letras'+meid).addClass('listaEdosActive');
                areaLastClicked=$area;
                //data.alwaysOn = !data.alwaysOn;
                data.alwaysOn = true;
                //$(this).data('maphilight', data).trigger('alwaysOn.maphilight');
                last_selected_id_estado = $(this).data('id-estado');
                cargarEstado(last_selected_id_estado);
        });

        $('.map').maphilight({ fade: true, strokeColor: '9f2241', fillColor: '9f2241', fillOpacity: 0.3 });//funciona, pero no cuando se redimienciona la imagen (cuando se cambia el estylo de la img con widt o height)                        
    });
    $('#descarga').click(function(e){
        e.preventDefault();
        //window.location = '/sintesisInformativa/entidad/'+id_estado;
        var href = '/sintesisInformativa/generaPDF';
        window.open(href);
    });
    $('.dropdown-trigger').dropdown();

    /// Incidencias ///
    $('#reportIncidencia').click(function(e){
        e.preventDefault();
        var href = '/incidencia/nuevaIncidencia';
        window.location = href;
    });
    $('#selectEdo').select2();
    $('#selectArea').select2();
    $('#selectSubArea').select2();
    $('#selectProp').select2();
    // Switch //
    $('#propuestaS').change(function() {      
        if($(this).prop('checked')){
          $('#flgPropuesta').val("1");
          $('.propuesta').show();
        }
        else{
          $('#flgPropuesta').val("0");
          //tweet//
          $('#txtTweet').val('');
          $('#txtTweet').removeAttr('name');
          $('#txtTweet').removeClass('validar');
          $('#tipoTweet').hide();
          //boletin//
          destory_editor('#txtBoletin');            
          $('#textoHtmlBoletin').val('');
          $('#textoHtmlBoletin').removeAttr('name');
          $('#textoHtmlBoletin').removeClass('validar');
          $('#textoBoletin').val('');
          $('#textoBoletin').removeAttr('name');
          $('#textoBoletin').removeClass('validar');
          $('#titBoletin').val('');
          $('#titBoletin').removeAttr('name');
          $('#titBoletin').removeClass('validar');
          $('#txtBoletin').empty();
          $('#tipoBoletin').hide();
          //select//
          $('.propuesta').hide();
          $('#selectProp').val('def').trigger('change');
        }
      });
      // Tipo propuesta //
    $('#selectProp').change(function(){
        var propuesta = $(this).val();
        console.log(propuesta);
        if(propuesta === '1'){ //Tweet
            //Boletin//
            $('#tipoBoletin').hide();            
            destory_editor('#txtBoletin');  
            $('#titBoletin').val('');
            $('#titBoletin').removeAttr('name');
            $('#titBoletin').removeClass('validar');          
            $('#textoHtmlBoletin').val('');
            $('#textoHtmlBoletin').removeAttr('name');
            $('#textoHtmlBoletin').removeClass('validar');
            $('#textoBoletin').val('');
            $('#textoBoletin').removeAttr('name');
            $('#textoBoletin').removeClass('validar');            
            //Tweet//
            $('#tipoTweet').show();    
            $('#txtTweet').attr('name', 'txtTweet');
            $('#txtTweet').addClass('validar');
            $('#txtBoletin').empty();     
        }
        if(propuesta === '2'){ //Boletin
            //Tweet//
            $('#tipoTweet').hide();
            $('#txtTweet').val('');
            $('#txtTweet').removeAttr('name');
            $('#txtTweet').removeClass('validar');
            //Boletin//
            $('#tipoBoletin').show();
            $('#titBoletin').attr('name', 'titBoletin');
            $('#titBoletin').addClass('validar');
            $('#textoHtmlBoletin').attr('name', 'textoHtmlBoletin');
            $('#textoHtmlBoletin').addClass('validar');
            $('#textoBoletin').attr('name', 'textoBoletin');
            $('#textoBoletin').addClass('validar');
            var quillBoletin = new Quill('#txtBoletin', {
                modules: {
                  toolbar: [
                    [{ header: [1, 2, 3, 4, 5, 6,  false] }],
                    ['bold', 'italic', 'underline','strike'],
                    [{ 'color': [] }, { 'background': [] }],
                    ['link'],
                    [{ 'script': 'sub'}, { 'script': 'super' }],
                    [{ 'list': 'ordered'}, { 'list': 'bullet' }], 
                             
                  ]
                },      
                theme: 'snow'  // or 'bubble'
              });      
              $('.ql-snow select').addClass('browser-default');  
              quillBoletin.on('text-change', function(delta, source) {
                  updateHtmlOutput_msj()
              })        
              $('#btn-convert').on('click', () => { updateHtmlOutput_msj() })        
                  function getQuillHtml_msj() { return quillBoletin.root.innerHTML; }
                  function updateHtmlOutput_msj()
                  {
                      let html = getQuillHtml_msj();
                      //console.log ( html );            
                      $('#textoHtmlBoletin').val(html);
                      var contenido = $('#textoHtmlBoletin').val();
                      var texto = contenido.replace(/<[^>]*>?/g, '');
                      $('#textoBoletin').val(texto);  
                  }
              updateHtmlOutput_msj();
              //quillBoletin.setContents([{ insert: '' }]);
        }
    })
    $('#guardaIncidecnia').click(function(e){
        e.preventDefault()
        var estado = $('#selectEdo').parsley();
        var area = $('#selectArea').parsley();
        var subArea = $('#selectSubArea').parsley();
        if($('#textoHtml').val()==='<p><br></p>'){
            $('#textoHtml').val('');
        }                                                          
        var campos = $('#incidenciaForm').find('.validar').parsley();
        //console.log(campos);
        var ctl = 0;
        for(const i in campos){
            if(campos[i].isValid() && estado.isValid()){
                ctl++
            }
            else{
                campos[i].validate();
                estado.validate();
                area.validate();
                subArea.validate();
            }
            if(ctl === campos.length){                            
                var formdata = new FormData($("#incidenciaForm")[0]);
                Swal.fire({
                  title: 'Enviando...',
                  allowEscapeKey: false,
                  allowOutsideClick: false,
                })
                Swal.showLoading();
                $.ajaxSetup({
                    headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                  });
                $.ajax({
                  url: "../incidencia/guardaIncidencia",
                  type: 'POST',
                  dataType: 'json',
                  data: formdata,                
                  cache:false,
                  processData: false,
                  contentType: false,
                })
                .done(function(result) {
                   console.log(result)               
                    Swal.fire(
                      'Guardado',
                      'Los datos han sido registrados',
                      'success'
                    )
                    var delayInMilliseconds = 2000; //1 second
                    setTimeout(function() {
                        //location.reload();
                        $("#incidenciaForm").trigger('reset');                        
                        $('#selectEdo').val('def').trigger('change');
                        $('#selectArea').val('def').trigger('change');
                        $('#selectSubArea').val('def').trigger('change');                        
                        quillNota.setContents([{ insert: '' }]);                    
                        $('.dropify-clear').click();
                        location.reload();
                        /* $( "#notasForm" ).steps('reset'); */
                        //var form = $("#notasForm").steps();
                    }, delayInMilliseconds);
                })
                .fail(function(result) {
                    console.log(result);
                    Swal.fire({
                      icon: 'error',
                      title: 'Oops...',
                      text: result.responseJSON,
                    })                
                })
                .always(function() {
                    console.log('complete');
                });

            }
        }

    });
    // Mapa dinamico Incidecnia//
    /* State Names */    
    var state_names = new Array();
    var state_class = new Array();
        state_names[1]="Aguascalientes";state_names[2]="Baja California";state_names[3]="Baja California Sur";state_names[4]="Campeche";state_names[5]="Coahuila";state_names[6]="Colima";state_names[7]="Chiapas";state_names[8]="Chihuahua";state_names[9]="Distrito Federal";state_names[10]="Durango";state_names[11]="Guanajuato";state_names[12]="Guerrero";state_names[13]="Hidalgo";state_names[14]="Jalisco";state_names[15]="Estado de M&eacute;xico";state_names[16]="Michoac&aacute;n";state_names[17]="Morelos";state_names[18]="Nayarit";state_names[19]="Nuevo Le&oacute;n";state_names[20]="Oaxaca";state_names[21]="Puebla";state_names[22]="Quer&eacute;taro";state_names[23]="Quintana roo";state_names[24]="San Luis Potos&iacute;";state_names[25]="Sinaloa";state_names[26]="Sonora";state_names[27]="Tabasco";state_names[28]="Tamaulipas";state_names[29]="Tlaxcala";state_names[30]="Veracruz";state_names[31]="Yucat&aacute;n";state_names[32]="Zacatecas";
        state_class[1]="AGU";state_class[2]="BCN";state_class[3]="BCS";state_class[4]="CAM";state_class[5]="COA";state_class[6]="COL";state_class[7]="CHP";state_class[8]="CHH";state_class[9]="DIF";state_class[10]="DUR";state_class[11]="GUA";state_class[12]="GRO";state_class[13]="HID";state_class[14]="JAL";state_class[15]="MEX";state_class[16]="MIC";state_class[17]="MOR";state_class[18]="NAY";state_class[19]="NLE";state_class[20]="OAX";state_class[21]="PUE";state_class[22]="QUE";state_class[23]="ROO";state_class[24]="SLP";state_class[25]="SIN";state_class[26]="SON";state_class[27]="TAB";state_class[28]="TAM";state_class[29]="TLA";state_class[30]="VER";state_class[31]="YUC";state_class[32]="ZAC";
    $(function () {
        $('.listaEdos').mouseover(function(e) {                
            $($(this).data('parent-map')).mouseover();
        }).mouseout(function(e) {                
            $($(this).data('parent-map')).mouseout();                    
        }).click(function(e) { 
            e.preventDefault(); 
            $($(this).data('parent-map')).click(); 
        });

    
        $('.areaIc').hover(function () {
            var id_estado = $(this).data('id-estado');
            var meid = $(this).attr('id');
            $('#edo').html(state_names[id_estado]);                
            $('#letras'+meid).addClass('listaEdosHover');
            $('.escudo').addClass('escudo_img');
            if(last_selected_id_estado!==null){
                $('.escudo').removeClass(state_class[last_selected_id_estado]);
            }
            $('.escudo').addClass(meid);
        }).mouseout(function () {
            var meid = $(this).attr('id');
            $('#letras'+meid).removeClass('listaEdosHover');
            $('.escudo').removeClass(meid);
            if(last_selected_id_estado!==null){
                $('#edo').html(state_names[last_selected_id_estado]);
                $('.escudo').addClass(state_class[last_selected_id_estado]);
            }else{                    
                $('#edo').html("&nbsp;");
                $('.escudo').removeClass('escudo_img');
                //$('.escudo').attr('class','escudo');
            }
        });
        //$('#map_ID').imageMapResize();//funciona perfectamente
        var areaLastClicked=null;
        var last_selected_id_estado=null;
        $('.areaIc').click(function (e) {
                e.preventDefault();
                var $area = $(this);
                //console.log($area)
                var meid = $area.attr('id');
                //console.log(meid)
                //$('.area').mouseout();
                var data = $area.data('maphilight') || {};    
                //console.log(data);                
                if(areaLastClicked!==null){                        
                    var lastData = areaLastClicked.data('maphilight') || {};
                    lastData.alwaysOn=false;
                    $('#letras'+areaLastClicked.attr('id')).removeClass('listaEdosActive');
                    $('.escudo').removeClass(state_class[last_selected_id_estado]);
                }
                $('#letras'+meid).addClass('listaEdosActive');
                areaLastClicked=$area;
                //data.alwaysOn = !data.alwaysOn;
                data.alwaysOn = true;
                //$(this).data('maphilight', data).trigger('alwaysOn.maphilight');
                last_selected_id_estado = $(this).data('id-estado');
                cargarEstadoIc(last_selected_id_estado);
        });

        $('.map').maphilight({ fade: true, strokeColor: '9f2241', fillColor: '9f2241', fillOpacity: 0.3 });//funciona, pero no cuando se redimienciona la imagen (cuando se cambia el estylo de la img con widt o height)                        
    });

    //select anidado incidencias //
    $('#selectArea').change(function(){
        $('#selectSubArea').val('def').trigger('change');
        var subAreaArr = [];
        var area = $(this).val();
        //console.log(area);
        $.ajax({
            url: "../incidencia/listaSubAreas/"+area,
            type: 'GET',
            cache:false,
        })
        .done(function(result) {
            //console.log(result)
            //return false;
            for(const i in result){
                if(result[i] != null){
                    subAreaArr.push({
                         id:result[i].idSubArea,
                         subArea:result[i].nomSubArea,                                                 
                    })
                }                 
           }
           var cadCom = '<option disabled selected value="def">Sub-Área</option>'; 
           for(const i in subAreaArr){
              cadCom += '<option value="'+subAreaArr[i].id+'">'+subAreaArr[i].subArea
                   + '</option>';  
           }
          //console.log(cadCom);
           $('#selectSubArea').empty()
           $('#selectSubArea').append(cadCom);           
 
         })
         .fail(function(result) {
             console.log(result)
         })
         .always(function() {
             console.log('complete');
         });
    });
    $('#descargaIncidencia').click(function(e){
        e.preventDefault();
        //window.location = '/sintesisInformativa/entidad/'+id_estado;
        var href = '/incidencias/generaPDFIncidencias';
        window.open(href);
    });
    
});