@extends('admin.master')
@section('css')

@endsection
@section('contenido')
    <h1>Denuncias</h1>
    <style>
        td.details-control {
            background: url('{{ URL::asset('assets/images/details_open.png') }}') no-repeat center center;
            cursor: pointer;
        }

        tr.shown td.details-control {
            background: url('{{ URL::asset('assets/images/details_close.png') }}') no-repeat center center;
        }

    </style>
    <input hidden id="idNota" type="text">
    <div class="row no-m-t no-m-b">
        <div class="card">
            <div class="card-content">
                <table id="tableDenuncias" class="display responsive-table datatable-example">
                    <thead>
                        <tr>
                            <th>&nbsp&nbsp&nbsp</th>
                            <th>ID</th>
                            <th scope="col">Expediente</th>
                            <th scope="col">Fuente</th>
                            <th scope="col">Atiende</th>
                            <th scope="col">Fecha de Denuncia</th>
                            <th scope="col">Estatus</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    <!-- Modal Structure -->
    <div id="modalOrden" class="modal">
        
    </div>
@endsection
@section('scripts')

@endsection
