@php
    //dd(url());
@endphp
<header class="mn-header navbar-fixed">
    <nav class="miNavHeader">
        <div class="nav-wrapper row">
            <section class="material-design-hamburger navigation-toggle">
                <a href="javascript:void(0)" data-activates="slide-out" class="button-collapse show-on-large material-design-hamburger__icon">
                    <span class="material-design-hamburger__layer"></span>
                </a>
            </section>
            <div class="header-title col s3 m3">      
                {{-- <span class="chapter-title">SIGEMINC</span> --}}
                <img src="{{URL::asset('assets/images/Logo_gob.png')}}" height="45px">
            </div>            
        </div>
    </nav>
</header>
<aside id="slide-out" class="side-nav white fixed">
    <div class="side-nav-wrapper">
        <div class="sidebar-profile">
            <div class="sidebar-profile-image">                
                <img src="{{URL::asset('assets/images/user.png')}}" class="circle" alt="">
            </div>
            <div class="sidebar-profile-info">                
                    <p>{{-- {{ Auth::user()->name }} --}}</p>                    
            </div>
        </div>        
    <ul class="sidebar-menu collapsible collapsible-accordion" data-collapsible="accordion">
        <li class="no-padding">
            <a class="waves-effect waves-grey" href="{{url('/denuncias')}}"><i class="material-icons">settings_input_svideo</i>Denuncias</a>
        </li>            
        {{-- <li class="no-padding">
            <a class="collapsible-header waves-effect waves-grey"><i class="material-icons">add_circle_outline</i>Agregar<i class="nav-drop-icon material-icons">keyboard_arrow_right</i></a>
            <div class="collapsible-body">
                <ul>
                    <li><a href="{{url('/nuevaNota')}}">Nota</a></li>
                    <li><a href="{{url('/nuevasOchoColumnas')}}">Ocho Columnas</a></li>
                    <li><a href="{{url('/nuvoBoletin')}}">Boletín</a></li>
                    <li><a href="{{url('/nuevaInfoAmbiental')}}">Información Ambiental</a></li>                    
                </ul>
            </div>
        </li>
        <li class="no-padding">
            <a class="collapsible-header waves-effect waves-grey"><i class="material-icons">visibility</i>Ver<i class="nav-drop-icon material-icons">keyboard_arrow_right</i></a>
            <div class="collapsible-body">
                <ul>
                    <li><a href="{{url('/notas')}}">Notas</a></li>                    
                    <li><a href="{{url('/ochoColumnas')}}">Ocho Columnas</a></li>
                    <li><a href="{{url('/boletines')}}">Boletín</a></li>
                    <li><a href="{{url('/ambiental')}}">Información Ambiental</a></li>  
                    <li><a href="{{'/incidencias'}}">Incidencias</a></li>                  
                </ul>
            </div>
        </li>
        <li class="no-padding">
            <a class="collapsible-header waves-effect waves-grey"><i class="material-icons">check_circle_outline</i>Validar<i class="nav-drop-icon material-icons">keyboard_arrow_right</i></a>
            <div class="collapsible-body">
                <ul>
                    <li><a href="{{url('/validaPdf')}}" target="_blank">Síntesis Informativa</a></li>                    
                    <li><a href="{{url('/validaPdfIncidencias')}}" target="_blank">Reporte de Incidencias</a></li>
                                      
                </ul>
            </div>
        </li> --}}        
        
        <li class="divider"></li>
        {{-- <li class="no-padding">            
            <a class="waves-effect waves-grey" href="{{ route('logout') }}"
               onclick="event.preventDefault();
                             document.getElementById('logout-form').submit();">
                <i class="material-icons">exit_to_app</i>Salir
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        </li> --}}
    </ul>   
    </div>
</aside>