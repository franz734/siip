<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;
use App\Denuncia;
use App\Entidad;

class DenunciaController extends Controller
{
    /// Datatable Denuncias ///
    public function getDenuncias(){
        $denuncias = Denuncia::getAllDenuncias();        
        //dd($denuncias);
        return datatables()->of($denuncias)->make(true);
    }
}
