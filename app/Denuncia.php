<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Denuncia extends Model
{
    protected $guarded = [];
    protected $table = 'denuncia';
    protected $connection = 'mysql';
    protected $primaryKey = 'idDenuncia';    
    const CREATED_AT = 'fcCrea';
    const UPDATED_AT = 'fcActualiza';
    const DELETED_AT = 'fcBorra';
    use SoftDeletes;
    /* use SoftDeletes;
    protected $date; */

    ////////////////////////
    ///*** Relaciones ***///
    ///////////////////////

    public function fuenteRel(){
        return $this->belongsTo('App\Fuente', 'idFuente');
    }
    public function EstatusRel(){
        return $this->belongsTo('App\Estatus', 'idEstatus');
    }
    public function datosParteRel(){
        return $this->hasMany('App\DatosPartes', 'idDenuncia');
    }    
    public function ordenRel(){
        return $this->hasOne('App\Orden', 'idDenuncia');
    }
    public function actaRel(){
        return $this->hasOne('App\Acta', 'idDenuncia');
    }

    ///////////////////////
    ///*** Funciones ***///
    //////////////////////
    
    /// Obtiene todas las denuncias ///
    static public function getAllDenuncias(){
        $denuncias = Denuncia::with(['fuenteRel', 'EstatusRel', 'datosParteRel.entidadRel', 'ordenRel', 'actaRel'])->get();             
        return $denuncias;
    }

    /////////////////////
    ///*** Metodos ***///
    ////////////////////
}
