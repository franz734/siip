<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Acta extends Model
{
    protected $guarded = [];
    protected $table = 'acta';
    protected $connection = 'mysql';
    protected $primaryKey = 'idActa';    
    const CREATED_AT = 'fcCrea';
    const UPDATED_AT = 'fcActualiza';
    const DELETED_AT = 'fcBorra';
    use SoftDeletes;
    /* use SoftDeletes;
    protected $date; */

    ////////////////////////
    ///*** Relaciones ***///
    ///////////////////////

    ///////////////////////
    ///*** Funciones ***///
    //////////////////////

    /////////////////////
    ///*** Metodos ***///
    ////////////////////

}
